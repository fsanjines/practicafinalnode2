// { json } es bodyparser
import express, { json } from 'express';
import morgan from 'morgan';

//Importing routes
import projectRoutes from './routes/projects';
import taskRoutes from './routes/task';
import usuariosRoutes from './routes/users';
import modulosRoutes from './routes/modulos';
import permisosRoutes from './routes/permisos';
import authorizationRoutes from './routes/authorization';


// inicialiando
const app = express();

//Middlewares
app.use(morgan('dev'));
app.use(json());


//Routes
app.use('/api/projects',projectRoutes);
app.use('/api/task', taskRoutes);
app.use('/api/users', usuariosRoutes);
app.use('/api/modulos', modulosRoutes);
app.use('/api/permisos', permisosRoutes);
app.use('/auth', authorizationRoutes);




export default app;