import Users from "../models/Users";
import Permisos from "../models/Permisos";

//Crea un usuario nuevo - OK
export async function createUser(req, res) {
    try {
        const { usuario, contrasena } = req.body;
        let newUser = await Users.create({
            usuario, contrasena
        }, {fields: ['usuario', 'contrasena']
        });
        res.json({
            message: 'Usuario Creado',
            data: newUser
        }) 
    } catch (error) {
        console.log(error);
    }
}
// Elimina Usuario - OK
export async function deleteUser(req, res) {
    try {
        const { id } = req.params;
        const deleteRowCount = await Users.destroy({
            where: {
                id
            }
        });
        res.json({ message: 'Usuarios eliminados:', deleteRowCount });
    } catch (error) {
        console.log(error);
    }
}

// Consulta un usuario por id - OK
export async function getOneUser(req, res) {
    try {
        const { id } = req.params;
        const user = await Users.findOne({
            where: { id },
            attributes: ['id', 'usuario', 'contrasena']
        })
        res.json(user); 
    } catch (error) {
        console.log(error);
    }
}
// api/users/permisos/id lista los modulos que tiene acceso el usuario
export async function getPermisosUser(req, res) {
    try {
        const { id } = req.params;
        const permisos = await Permisos.findAll({
            attributes: ['id', 'moduloid'],
            where: { usuario: id }
        })
        res.json({permisos});
    } catch (error) {
        console.log(error);
    }
}

// Lista usuarios - OK
export async function getUsers(req, res) {
    try {
        const users = await Users.findAll({
            attributes: ['id', 'usuario', 'contrasena']
        });
        res.json({users});
    } catch (error) {
        console.log(error);
    }
}
// Actualizacion de usuario - OK
export async function updateUser(req, res) {
    try {
        const { id } = req.params;
        const { usuario, contrasena } = req.body;
        const usuarioOne = await Users.findOne({
            attributes: ['usuario', 'contrasena'],
            where: {id}
        })
        const updatedUser = await Users.update({
            usuario, contrasena
        }, {
            where: { id }
        })
        res.json({
            message: 'Usuario Actualizado',
            updatedUser
        })
    } catch (error) {
        console.log(error);
    }
}
 
//Usuario y password
export async function login(req, res) {
    const message = "";
    try {
        const { usuario, contrasena } = req.body;
        //console.log(usuario, contrasena)
        if (!usuario || !contrasena) 
        {
            res.status(405).json({
                message: "Debe enviar usuario y contraseña"
            })
        }
        else 
        {
            const user = await Users.findOne({
                where: { usuario, contrasena },
                attributes: ['id', 'usuario', 'contrasena']
            })
            if (user) {
                res.status(200).json({
                    "id": user.id,
                    "usuario": user.usuario,
                    "contrasena": user.contrasena,
                    "status": "OK"
                });
                console.log(user.id);
            }
            else
            {
                res.status(200).json({ message: "No existe usuario o contraseña"}); 
            }
            
        }
        return message
    } catch (error) {
        res.status(406).json({
            message: "Problemas con el servidor"
        })
    }
}