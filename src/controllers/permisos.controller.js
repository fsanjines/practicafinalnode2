import Permisos from "../models/Permisos";
import Users from "../models/Users";

// Crea permiso en base al nro de modulo y usuario - OK
export async function createPermiso(req, res) {
    try {
        const { moduloid, usuario } = req.body;
        let newPermiso = await Permisos.create({
            moduloid, usuario
        }, {fields: ['moduloid', 'usuario']
        });

        res.json({
            message: 'Permiso Adicionado',
            data: newPermiso
        })
    } catch (error) {
        console.log(error);
        
    }
}
export async function deletePermiso(req, res) {
    try {
        const { id } = req.params;
        const deleteRowCount = await Permisos.destroy({
            where: {
                id
            }
        });
        res.json({ message: 'Permisos eliminados:', deleteRowCount });
    } catch (error) {
        console.log(error);
   
    }
}
// Consulta un permiso por el ID del permiso
export async function getOnePermiso(req, res) {
    try {
        const { id } = req.params;
        const permiso = await Permisos.findOne({
            where: { id },
            attributes: ['id', 'moduloid', 'usuario']
        })
        res.json(permiso);
    } catch (error) {
        console.log(error);
    }
}
// Consulta http://localhost:3000/api/permisos/users/1 el usuario 1 a que tiene permisos
export async function getUserPermisos(req, res) {
    try {
        const { id } = req.params;
        const usuarios = await Permisos.findAll({
            attributes: ['id', 'moduloid', 'usuario'],
            where: { usuario: id }
        })
        res.json({usuarios});
    } catch (error) {
        console.log(error);
        
    }
}

// Lista permisos - OK
export async function getPermisos(req, res) {
    try {
        const permisos = await Permisos.findAll({
            attributes: ['id', 'moduloid', 'usuario']
        });
        res.json({permisos});
    } catch (error) {
        console.log(error);
        
    }
}
// Actualiza http://localhost:3000/api/permisos/6
/* {
	"moduloid": "5",
	"usuario": "3"
}*/
export async function updatePermiso(req, res) {
    try {
        const { id } = req.params;
        const { moduloid, usuario } = req.body;
        const permiso = await Permisos.findOne({
            attributes: ['id', 'moduloid', 'usuario'],
            where: {id}
        })
        const updatedPermiso = await Permisos.update({
            moduloid, usuario
        }, {
            where: { id }
        })
        res.json({
            message: 'Permiso Actualizado',
            updatedPermiso
        })
    } catch (error) {
        console.log(error);
        
    }
}

