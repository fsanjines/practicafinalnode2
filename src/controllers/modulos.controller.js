import Modulos from "../models/Modulos";
import Permisos from "../models/Permisos";


//Creacion de Modulo OK
export async function createModulo(req, res) {
    try {
        const { modulo } = req.body;
        let newModulo = await Modulos.create({
            modulo
        }, {fields: ['modulo']
        });

        res.json({
            message: 'Modulo Creado',
            data: newModulo
        })
    } catch (error) {
        console.log(error);
    }
}
// Borra un modulo - OK
export async function deleteModulo(req, res) {
    try {
        const { id } = req.params;
        const deleteRowCount = await Modulos.destroy({
            where: {
                id
            }
        });
        res.json({ message: 'Modulos eliminadps:', deleteRowCount });
    } catch (error) {
        console.log(error);
    }
}
//Conmsulta un modulo - OK
export async function getOneModulo(req, res) {
    try {
        const { id } = req.params;
        const modulo = await Modulos.findOne({
            where: { id },
            attributes: ['id', 'modulo']
        })
        res.json(modulo);
    } catch (error) {
        console.log(error);
    }
}
//http://localhost:3000/api/modulos/users/1 el usuario tiene acceso a los modulos
export async function getUserModulos(req, res) {
    try {
        const { id } = req.params;
        const modulos = await Permisos.findAll({
            attributes: ['id', 'moduloid'],
            where: { usuario: id }
        })
        res.json({modulos});
    } catch (error) {
        console.log(error);
    }
}
//Lista los modulos - OK
export async function getModulos(req, res) {
    try {
        const modulos = await Modulos.findAll({
            attributes: ['id', 'modulo']
        });
        res.json({modulos});
    } catch (error) {
        console.log(error);
    }
}
// Actualiza un modulo - ok
export async function updateModulo (req, res) {
    try {
        const { id } = req.params;
        const { modulo } = req.body;
        const moduloOne = await Modulos.findOne({
            attributes: ['modulo'],
            where: {id}
        })
        const updatedModulo = await Modulos.update({
            modulo
        }, {
            where: { id }
        })
        res.json({
            message: 'Modulo Actualizado',
            updatedModulo
        })
    } catch (error) {
        console.log(error);
    }
}
