import Task from "../models/Task";

export async function createTask(req, res) {
    try {
        
        const { nombre, realizado, projectId } = req.body;
        console.log(projectId);
        let newTask = await Task.create({
            nombre, realizado, projectId
        }, {fields: ['nombre', 'realizado', 'projectId']
        });

        res.json({
            message: 'Tarea Creada',
            data: newTask
        })
    } catch (error) {
        console.log(error);
        
    }
}

export async function getTasks(req, res) {
    try {
        const tasks = await Task.findAll({
            attributes: ['id', 'nombre', 'realizado', 'projectId'],
            order: [
                ['id', 'DESC']
            ]
        });
        res.json({tasks});
    } catch (error) {
        console.log(error);
    }
}

export async function getOneTask(req, res) {
    try {
        const { id } = req.params;
        const task = await Task.findOne({
            where: { id },
            attributes: ['id', 'nombre', 'realizado', 'projectId']
        })
        res.json(task);
    } catch (error) {
        console.log(error);
    }
}

export async function updateTask(req, res) {
    try {
        const { id } = req.params;
        const { nombre, realizado, projectId } = req.body;
        const task = await Task.findOne({
            attributes: ['nombre', 'realizado', 'projectId'],
            where: {id}
        })
        const updatedTask = await Task.update({
            nombre, realizado, projectId
        }, {
            where: { id }
        })
        res.json({
            message: 'Tarea Actualizada',
            updatedTask
        })
    } catch (error) {
        console.log(error);
    }
}

export async function deleteTask(req, res) {
    try {
        const { id } = req.params;
        const deleteRowCount = await Task.destroy({
            where: {
                id
            }
        });
        res.json({ message: 'Tareas eliminadas:', deleteRowCount });

    } catch (error) {
        console.log(error);
    }
}

export async function getTaskByProject(req, res) {
    try {
        const { projectId } = req.params;
        const tasks = await Task.findAll({
            attributes: ['id', 'nombre', 'realizado', 'projectId'],
            where: { projectId }
        })
        res.json({tasks});
    } catch (error) {
        console.log(error);
    }
}