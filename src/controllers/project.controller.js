import Project from '../models/Project';

export async function createProject(req, res) {
    console.log(req.body);
    const {nombre, prioridad, descripcion, fecha_entrega} = req.body;
    try {
        let newProject = await Project.create({
            nombre, 
            prioridad,
            descripcion,
            fecha_entrega
        }, {fields: [
            'nombre', 
            'prioridad',
            'descripcion',
            'fecha_entrega'
        ]
        });
        if (newProject) {
            return res.json({
                message: 'Proyecto creado satisfactoriamente',
                data: newProject
            });
        }
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            message: 'Algo salio mal',
            data: {}
        })
    }
};

export async function getProjects(req, res) {
    try {
        const projects = await Project.findAll();
            res.json({
            data: projects
        });    
    } catch (error) {
        console.log(error);
    }
};

export async function getOneProject(req, res) {
    try {
        const { id } = req.params;
        const project = await Project.findOne({
        where: { id: id }
        });
        res.json({
            data: project
        });    
    } catch (error) {
        console.log(error);
    }
    
};

export async function deleteOneProject(req, res) {
    try {
        const { id } = req.params;
        const deleteRowCount = await Project.destroy({
        where: { 
            id: id 
        }
        }); 
        res.json({
            message: 'Proyecto eliminado satisfactoriamente',
            count: deleteRowCount
        })  
    } catch (error) {
        console.log(error);
    }
};

export async function updateProject(req, res) {
    try {
        const { id } = req.params;
        console.log('ID:', id);
        const {nombre, prioridad, descripcion, fecha_entrega} = req.body;

        const projects = await Project.findAll({
            attributes: ['id', 'nombre', 'prioridad', 'descripcion', 'fecha_entrega'],
            where: {
                id
            }
        });
        if (projects.length > 0) {
            projects.forEach(async project => {
                await project.update({
                    nombre, 
                    prioridad,
                    descripcion,
                    fecha_entrega
                });
            });
        }
        return res.json({
            message: 'Proyecto actualizado',
            data: projects
        });
    } catch (error) {
        console.log(error);
    }
};