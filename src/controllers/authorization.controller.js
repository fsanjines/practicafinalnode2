const express = require('express');
const app = express();
require('dotenv').config()

const jwt = require('jsonwebtoken');

app.use(express.json());

// POST http://localhost:3000/auth/token - OK
export async function token(req, res)
{
    const refreshToken = req.body.token
    const usuario = req.body.usuario
    if (refreshToken == null) return res.sendStatus(401)
    if (!refreshToken.includes(refreshToken)) return res.sendStatus(403)
    await jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err) =>{
        if (err) return res.sendStatus(403)
        const accessToken = jwt.sign(usuario, process.env.ACCESS_TOKEN_SECRET) 
        res.json({ accessToken: accessToken })
    })
};

// login y da token - ok
export async function login(req, res)
{
    //Autenticar usuario
    const { usuario } = req.body;
    const accessToken = jwt.sign(usuario, process.env.ACCESS_TOKEN_SECRET) 
    const refreshToken = await jwt.sign(usuario, process.env.REFRESH_TOKEN_SECRET)
    res.json({ accessToken: accessToken, refreshToken: refreshToken })
};

export async function logout(req, res)
{
    res.sendStatus(204)
}

export async function tokenVerify(req, res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader &&  authHeader.split(' ')[1]
    if (token == null ) return res.sendStatus(401)

    await jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, usuario)=>{
        console.log('------------------------')
        console.log(err, usuario)
        console.log('------------------------')
        
        if (err) return res.sendStatus(403)
        req.usuario = usuario
        res.json({
            "usuario": usuario,
            "token": token
        })
        next()
    })
}