import { Router } from 'express';
const router = Router();

import { createTask, deleteTask, getOneTask, getTaskByProject, getTasks, updateTask } from '../controllers/task.controller';
router.post('/', createTask);
router.get('/', getTasks);
router.delete('/:id', deleteTask);
router.put('/:id', updateTask);
router.get('/:id', getOneTask);

//api/task/project/projectID
router.get('/project/:projectId', getTaskByProject);

export default router;