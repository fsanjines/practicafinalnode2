import { Router } from 'express';
const router = Router();

import { createUser, deleteUser, getOneUser, getPermisosUser, getUsers, updateUser, login } from '../controllers/users.controller';
router.post('/', createUser);
router.get('/', getUsers);
router.delete('/:id', deleteUser);
router.put('/:id', updateUser);
router.get('/:id', getOneUser);
router.post('/login', login);

router.get('/permisos/:id', getPermisosUser);

export default router;