import { Router } from 'express';
const router = Router();

import { createModulo, deleteModulo, getOneModulo, getUserModulos, getModulos, updateModulo } from '../controllers/modulos.controller';
router.post('/', createModulo);
router.get('/', getModulos);
router.delete('/:id', deleteModulo);
router.put('/:id', updateModulo);
router.get('/:id', getOneModulo);

router.get('/users/:id', getUserModulos);

export default router;