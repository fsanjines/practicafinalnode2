import { Router } from 'express';
const router = Router();

import { createPermiso, deletePermiso, getOnePermiso, getUserPermisos, getPermisos, updatePermiso } from '../controllers/permisos.controller';
router.post('/', createPermiso);
router.get('/', getPermisos);
router.delete('/:id', deletePermiso);
router.put('/:id', updatePermiso);
router.get('/:id', getOnePermiso);

router.get('/users/:id', getUserPermisos);

export default router;