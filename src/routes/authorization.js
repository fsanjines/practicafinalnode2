import { Router } from 'express';
const router = Router();

import { token, login, logout, tokenVerify } from '../controllers/authorization.controller';
router.post('/token', token);
router.post('/login', login);
router.delete('/logout', logout);
router.get('/verify', tokenVerify);

export default router;