import Sequelize from 'sequelize';
import { sequelize } from '../database/database';


const Modulos = sequelize.define('modulos', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    modulo: {
        type: Sequelize.TEXT
    }
}, {
    freezeTableName: true,
    tableName: "modulos",
    timestamps: false
});


export default Modulos;