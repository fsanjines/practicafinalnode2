import Sequelize from 'sequelize';
import { sequelize } from '../database/database';

const Users = sequelize.define('users', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    usuario: {
        type: Sequelize.TEXT
    },
    contrasena: {
        type: Sequelize.TEXT
    }
}, {
    freezeTableName: true,
    tableName: "usuarios",
    timestamps: false
});


export default Users;