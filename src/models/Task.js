import Sequelize from 'sequelize';
import { sequelize } from '../database/database';


const Task = sequelize.define('task', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    nombre: {
        type: Sequelize.TEXT
    },
    realizado: {
        type: Sequelize.BOOLEAN
    },
    projectId: {
        type: Sequelize.INTEGER
    }
}, {
    timestamps: false
});


export default Task;