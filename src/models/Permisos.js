import Sequelize from 'sequelize';
import { sequelize } from '../database/database';

/*import Users from './Users';
import Modulos from './Modulos';*/


const Permisos = sequelize.define('permisos', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    moduloid: {
        type: Sequelize.INTEGER
    },
    usuario: {
        type: Sequelize.INTEGER
    }
}, {
    freezeTableName: true,
    tableName: "permisos",
    timestamps: false
});

/*
Permisos.hasMany(Users, { foreingKey: 'id', sourceKey: 'usuario'});
Permisos.hasMany(Modulos, { foreingKey: 'id', sourceKey: 'moduloid'});
*/

export default Permisos;